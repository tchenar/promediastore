<?php

// Ajout dune variable
$var-envir = true;
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'bdacemschool');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kX5;hDI5GP0q.u+?7yh>~P.jFj$PyarF$=I0c8|N!AUHoOJs3%9q x+aCs~FI/3L');
define('SECURE_AUTH_KEY',  'F]n6:-WWUK}UHDg-0OY1Rb$sFaR@Jqx(!@pu-BUj*].S`1?DYPllNAaOi7}`).}u');
define('LOGGED_IN_KEY',    'Z$jLI;c9QxPaT0V9&24GZuI<Lc=,nR0h(B+2NRp_Ntg/s@ZNt4J.|tO5>Q3JXouD');
define('NONCE_KEY',        'l#~4jsN/uurrVKiEms:.(WCI`?E]1MvUaUcL?NWPR6d1.d#tqGW_`9eNNc2lN4O%');
define('AUTH_SALT',        '=EccYEdw%et&zqKv5=ae +W@!8!cNcTTrd`Q|_;%kVdcE@D$M-2IHlXpA2ZvO(E^');
define('SECURE_AUTH_SALT', '`;YDC#Tvo3QOy3reB3ZU]/e,;cK81U5L+p&`.P19D[DG1,3jw/?ySZOdRrk{Qm>2');
define('LOGGED_IN_SALT',   '6B)pqbnJ}_DmF%(&Go}%3p/V4YpJUWe3[=4GRwiSK9:YbWi6#^u<t4;oz3Yy%D1Z');
define('NONCE_SALT',       't]H$thH^{1:ZxCYO -mf!E0@R_T;v&j{<?a`6y1tc-$42_EWEk#L.]b&?vnb:`bB');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');